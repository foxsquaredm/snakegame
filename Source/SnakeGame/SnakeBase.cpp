// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/World.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 50.f;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementSpeed = 10.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	UE_LOG(LogTemp, Display, TEXT("I just started running ==============================================="));

	Super::BeginPlay();
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//Move(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElemensNum)
{
	for (int i = 0; i < ElemensNum; ++i)
	{
		FTransform NewFTransform;
		if (ElemensNum == 1)
		{
			auto PrevElement = SnakeElements[SnakeElements.Num() - 1];
			FVector PrevLocation = PrevElement->GetActorLocation();
			NewFTransform = FTransform(PrevLocation);
		}
		else
		{
			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			NewFTransform = FTransform(NewLocation);
		}

		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewFTransform);
		//NewSnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElement->SnakeOwner = this;
		//NewSnakeElement->SetActorHiddenInGame(true);

		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		//NewSnakeElement->SetActorHiddenInGame(false);
	}
}

void ASnakeBase::RemoveSnakeElement()
{
	const auto LastElement = SnakeElements.Last();
	SnakeElements.Remove(LastElement);
	LastElement->Destroy();
}

//void ASnakeBase::Move(float DeltaTime)
void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	////float MovementSpeedDelta = MovementSpeed * DeltaTime;
	////float MovementSpeed = ElementSize;
	//MovementSpeed = ElementSize;

	SetActorTickInterval(MovementSpeed);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		////MovementVector.X += MovementSpeedDelta;
		//MovementVector.X += MovementSpeed;
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		////MovementVector.X -= MovementSpeedDelta;
		//MovementVector.X -= MovementSpeed;
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		////MovementVector.Y += MovementSpeedDelta;
		//MovementVector.Y += MovementSpeed;
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		////MovementVector.Y -= MovementSpeedDelta;
		//MovementVector.Y -= MovementSpeed;
		MovementVector.Y -= ElementSize;
		break;

		//default:
		//	break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() -1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

		//CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor* Other)
{
	{
		if (IsValid(OverlappedElement))
		{
			int32 ElemIndex;
			SnakeElements.Find(OverlappedElement, ElemIndex);
			bool bIsFirst = ElemIndex == 0;

			IInteractable* InteractableInterface = Cast<IInteractable>(Other);
			if (InteractableInterface)
			{
				InteractableInterface->Interact(this, bIsFirst);
			}
		}
	}
}

