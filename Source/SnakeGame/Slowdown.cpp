// Fill out your copyright notice in the Description page of Project Settings.


#include "Slowdown.h"
#include "SnakeBase.h"
#include "Engine/Engine.h"

// Sets default values
ASlowdown::ASlowdown()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SizeOfIncrease = 0.1f;
}

// Called when the game starts or when spawned
void ASlowdown::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowdown::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowdown::Interact(AActor * Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	float PreviousSpeed = Snake->MovementSpeed;

	if (IsValid(Snake))
	{
		if (Snake->MovementSpeed < 1.0f)
		{
			Snake->MovementSpeed += SizeOfIncrease;
		}
	}

	//std::string someString = "Hello!";

	float CurrentSpeed = Snake->MovementSpeed;
	//UE_LOG(LogTemp, Warning, TEXT("CurrentSpeed = %f"), , CurrentSpeed);
	UE_LOG(LogTemp, Warning, TEXT("PreviousSpeed = %f, CurrentSpeed = %f"), PreviousSpeed, CurrentSpeed);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("This is the log!"));

	Destroy();
}